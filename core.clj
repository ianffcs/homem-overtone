(ns first-try.core)

(use 'overtone.live)

(definst foo [] (saw 220))

(foo)

(definst bar [freq 220] (saw freq))

(bar 110)

(definst baz [freq 440] (* 0.3 (saw freq)))

(baz 220)

(definst quux [freq 440] (* 0.3 (saw freq)))

(quux)

(ctl quux :freq 100)

(definst trem [freq 440 depth 10 rate 6 length 10]
  (* 0.3
     (line:kr 0 1 length FREE)
     (saw (+ freq (* depth (sin-osc:kr rate))))))

(trem)

(trem 200 60 0.8)

(stop)
