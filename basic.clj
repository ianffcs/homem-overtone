(ns first-try.basic
  (:use overtone.live))

(defsynth foo [freq 200 dur 0.5]
  (let [src (saw [frew (* freq 1.01) (* 0.99 freq)])
        low (sin-osc (/ freq 2))
        filt (lpf src (line:kr (*10 freq) freq 10))
        env (env-gen (perc 0.1 dur) :action FREE)]
    (out 0 (pan2 (* 0.8 low env filt)))))

(foo 440)
